import unittest
import sys
sys.path.append('../client')
import client


class Test_Client(unittest.TestCase):
   
    def test_pick_a_command_from_user(self, mock_input):
        client_obj = client.Client()
        result = client_obj.pick_a_command_from_user()
        self.assertIsInstance(result, str, "Result type should be 'str' but it is not.")
        

unittest.main(argv=[''],verbosity=2,exit=False)
