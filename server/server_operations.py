import json
import time
from datetime import datetime

class Server_operations:
    
    def func_uptime(self, START_TIME):
        uptime = int(time.time() - START_TIME)
        seconds = int(uptime % 60)
        minutes = int(uptime / 60) % 60 
        hours = int(uptime / 60 / 60) % 24
        days = int(uptime / 60 / 60 / 24)
        dict_time = {
            "days" : days,
            "hours" : hours,
            "minutes" : minutes,
            "seconds" : seconds
            }
        return dict_time
        
    def func_info(self, START_TIME, SERVER_VERSION):
        start_time = datetime.fromtimestamp(START_TIME)
        year = start_time.year
        month = start_time.month
        day = start_time.day
        hour = start_time.hour
        minute = start_time.minute
        second = start_time.second
        
        dict_start_time = {
            "date": {
                "year" : year,
                "month" : month,
                "day" : day,
                "hour" : hour,
                "minute" : minute,
                "second" : second
                },
            "server_version" : SERVER_VERSION
            }
        return dict_start_time

    def func_help(self):
        dict_help = {
            "help":"prints a list of commands",
            "login <username> <password>":"Try to login with username and password.",
            "info": "prints a version of the server and the time when it was started",
            "uptime": "prints how long the server is up",
            "stop": "stops the server",
            "show_user <username>": "shows info about user",
            "create_user <username> <role> <password>": "Creating a new user.",
            "delete_user <username>": "deleting a user",
            "read_messages":"Read messages from your message box.",
            "send_message <receiver_name> <message_to_sent>":"Sending a message to user's message box. Message cannot be longer than 255 characters.",
            "delete_messages":"Delete all messages from your message box."
            }
        return dict_help



