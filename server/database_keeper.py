import psycopg

reader_password = "reader"
users_manager_password = "users_manager"
messages_manager_password = "messages_manager"

class Database_Keeper:
    # user management functions:
    def get_role_with_userID(self, userID):
        with psycopg.connect(f"dbname=zr_cs user=reader password={reader_password}") as conn:
            with conn.cursor() as cur:
                cur.execute(f"SELECT roles.name FROM users INNER JOIN roles ON users.role = roles.id WHERE users.id = {userID};")
                result = cur.fetchone()
                return result[0]
            
    def get_userID_with_username(self, login_username):
        with psycopg.connect(f"dbname=zr_cs user=reader password={reader_password}") as conn:
            with conn.cursor() as cur:
                cur.execute(f"SELECT id FROM users WHERE name = '{login_username}'")
                result = cur.fetchone()
                return result[0]

    def get_hash_with_userID(self, userID):
        with psycopg.connect(f"dbname=zr_cs user=users_manager password={users_manager_password}") as conn:
            with conn.cursor() as cur:
                cur.execute(f"SELECT hash FROM secret WHERE id = {userID};")
                result = cur.fetchone()
                return result[0]       

    def get_username_role_with_username(self, username):
        with psycopg.connect(f"dbname=zr_cs user=reader password={reader_password}") as conn:
            with conn.cursor() as cur:
                cur.execute(f"SELECT users.name, roles.name FROM users INNER JOIN roles ON users.role = roles.id WHERE users.name = '{username}';")
                result = cur.fetchone()
                return result

    def is_this_user_exists_already(self, username):
        with psycopg.connect(f"dbname=zr_cs user=reader password={reader_password}") as conn:
            with conn.cursor() as cur:
                cur.execute(f"SELECT name FROM users WHERE name = '{username}';")
                result = cur.fetchone()
                if type(result) == tuple and result[0] == username:
                    return True
                else:
                    return False

    def is_this_role_exists(self, role):
        with psycopg.connect(f"dbname=zr_cs user=reader password={reader_password}") as conn:
            with conn.cursor() as cur:
                cur.execute(f"SELECT name FROM roles WHERE name = '{role}';")
                result = cur.fetchone()
                if type(result) == tuple and result[0] == role:
                    return True
                else:
                    return False

    def generate_new_available_userID(self):
        with psycopg.connect(f"dbname=zr_cs user=users_manager password={users_manager_password}") as conn:
            with conn.cursor() as cur:
                cur.execute("SELECT MAX(id) FROM users;")
                maxID_from_users = cur.fetchone()
                cur.execute("SELECT MAX(id) FROM secret;")
                maxID_from_secret = cur.fetchone()
                if maxID_from_users > maxID_from_secret:
                    return maxID_from_users[0] + 1
                else:
                    return maxID_from_secret[0] + 1

    def add_new_records_to_users_and_secret(self, record_to_users, record_to_secret):
        with psycopg.connect(f"dbname=zr_cs user=users_manager password={users_manager_password}") as conn:
            with conn.cursor() as cur:
                cur.execute(f"SELECT id FROM roles WHERE name = '{record_to_users[2]}';")
                result = cur.fetchone()
                role_id = result[0]
                user_id = record_to_users[0]
                user_name = record_to_users[1]
                secret_id = record_to_secret[0]
                secret_hash = record_to_secret[1]
                cur.execute(f"INSERT INTO users (id, name, role) VALUES ({user_id}, '{user_name}', {role_id});")
                cur.execute(f"INSERT INTO secret (id, hash) VALUES ({secret_id}, '{secret_hash}');")

                return "A new record added to the table users."
            
    def delete_user_with_userID(self, userID):
        with psycopg.connect(f"dbname=zr_cs user=users_manager password={users_manager_password}") as conn:
            with conn.cursor() as cur:
                cur.execute(f"DELETE FROM secret WHERE id = {userID};")
                cur.execute(f"DELETE FROM users WHERE id = {userID};")
        return f"User with ID: '{userID}' deleted successfully."           

    # messages management functions:
    def get_all_messages_from_messages_table(self, message_box_owner_name):
        with psycopg.connect(f"dbname=zr_cs user=messages_manager password={messages_manager_password}") as conn:
            with conn.cursor() as cur:
                cur.execute(f"""
                                SELECT
                                        users_sender_name,
                                        message,
                                        date
                                FROM
                                        (SELECT
                                                message,
                                                name as users_sender_name,
                                                recepient,
                                                date
                                        FROM messages
                                        JOIN users
                                                ON messages.sender = users.id) AS subquery
                                JOIN users
                                        ON subquery.recepient = users.id
                                WHERE name = '{message_box_owner_name}'
                                ORDER BY date ASC;""")
                result = cur.fetchall()
                return result

    def remove_all_messages_of_a_user(self, message_box_owner_name):
        with psycopg.connect(f"dbname=zr_cs user=messages_manager password={messages_manager_password}") as conn:
            with conn.cursor() as cur:
                cur.execute(f"""DELETE FROM messages
                                WHERE recepient = (
                                            SELECT id
                                            FROM users
                                            WHERE name = '{message_box_owner_name}'
                                            );""")
                return "Messages deleted."
            
    def is_message_box_full(self, message_box_owner_name, MAX_NUM_OF_MESSAGES):
        with psycopg.connect(f"dbname=zr_cs user=messages_manager password={messages_manager_password}") as conn:
            with conn.cursor() as cur:
                cur.execute(f"""
                                SELECT COUNT(message)
                                FROM
                                        (SELECT
                                                message,
                                                name as users_sender_name,
                                                recepient,
                                                date
                                        FROM messages
                                        JOIN users
                                                ON messages.sender = users.id) AS subquery
                                JOIN users
                                        ON subquery.recepient = users.id
                                WHERE name = '{message_box_owner_name}';""")
                result = cur.fetchone()
                if result[0] >= MAX_NUM_OF_MESSAGES:
                    return True
                else:
                    return False

    def send_a_message(self, sender_name, receiver_name, message):
        with psycopg.connect(f"dbname=zr_cs user=messages_manager password={messages_manager_password}") as conn:
            with conn.cursor() as cur:
                cur.execute(f"""
                                INSERT INTO messages (message, sender, recepient)
                                VALUES ('{message}',
                                (SELECT id FROM users WHERE name = '{sender_name}'),
                                (SELECT id FROM users WHERE name = '{receiver_name}')
                                        );
                            """)
                return "Message sent."




















