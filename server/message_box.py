import files_keeper
import database_keeper

MAX_NUM_OF_MESSAGES = 5

class Message_Box:

    def read_messages(self, message_box_owner_name):
        obj_database_keeper = database_keeper.Database_Keeper()
        all_messages = obj_database_keeper.get_all_messages_from_messages_table(message_box_owner_name)
                
        all_messages_string = ""
        if len(all_messages) >= MAX_NUM_OF_MESSAGES:
            all_messages_string = all_messages_string + "Your message box is full!\nYou cannot receive any new message now.\nDelete messages from the message box in order to unlock possibility of receiving new messages.\n" 
        
        for line in all_messages:
            all_messages_string = all_messages_string + f"Message from '{line[0]}':\n{line[1]}\nDate: {line[2]}.\n"
        if all_messages_string == "":
            return "Your message box is empty."
        return all_messages_string

    def delete_messages(self, message_box_owner_name):
        obj_database_keeper = database_keeper.Database_Keeper()
        obj_database_keeper.remove_all_messages_of_a_user(message_box_owner_name)
        return f"All messages from message box of a user '{message_box_owner_name}' deleted."

    def send_message(self, sender_name, receiver_name, message):
        obj_database_keeper = database_keeper.Database_Keeper()

        # check whether the message is valid
        if type(message) is not str:
            return f"The message is invalid. Unable to send the message.\nThe message should be type of 'str' but it is {type(message)}."
        elif len(message) > 255:
            return f"The message is invalid. Unable to send the message.\nThe length of the message is {len(message)} but it should be max 255 chars."

        # check whther message box is full
        if obj_database_keeper.is_message_box_full(receiver_name, MAX_NUM_OF_MESSAGES):
            return f"Error: Unable to send the message. Messages box of a user '{receiver_name}' is full."
        
        # send the message
        obj_database_keeper.send_a_message(sender_name, receiver_name, message)
        return "The message sent."



