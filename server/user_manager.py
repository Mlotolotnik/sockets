import files_keeper
import random
import hashlib
import database_keeper

roles = {
	"admin":["help", "info", "uptime", "stop", "show_user", "create_user", "delete_user", "read_messages", "send_message", "delete_messages"],
	"user":["help", "info", "uptime", "stop", "show_user", "read_messages", "send_message", "delete_messages"]
}

class User_Manager:

    def is_access_to_function_allowed(self, userID, function_name):
        if userID == None:
            return False
        obj_database_keeper = database_keeper.Database_Keeper()
        role = obj_database_keeper.get_role_with_userID(userID)
        list_of_allowed_functions = roles[role]
        if function_name in list_of_allowed_functions:
            return True
        else:
            return False

    def hash_password(self, password):
        password_bytes = password.encode('utf=8')
        hash_object = hashlib.sha256(password_bytes)
        return hash_object.hexdigest()

    def log_in_user(self, login_username, login_password):
        try:
            obj_database_keeper = database_keeper.Database_Keeper()
            userID = obj_database_keeper.get_userID_with_username(login_username)
            hash_from_secret = obj_database_keeper.get_hash_with_userID(userID)
            login_hash = self.hash_password(login_password)
        except:
            return ["Error. Login Failed. Check your login and password and try again", None]
        if login_hash == hash_from_secret:
            unique_session_number = random.randint(1_000_000_000, 9_999_999_999)
            return [f"User '{login_username}' logged in successfully.", userID, unique_session_number, login_username]
        else:
            return ["Error. Login Failed. Check your login and password and try again", None]

    def show_user(self, username):
        obj_database_keeper = database_keeper.Database_Keeper()
        record = obj_database_keeper.get_username_role_with_username(username)
        if record is None:
            return f"Error: Unable to find such user: '{username}'."
        result = f"Username: '{record[0]}'. Role: '{record[1]}'."
        return result

    def create_user(self, username, role, password):
        #initial preparation and checks:
        obj_database_keeper = database_keeper.Database_Keeper()
        
        if obj_database_keeper.is_this_user_exists_already(username):
            return f"Error: User '{username}' exists already. Try choose another username."
        if not obj_database_keeper.is_this_role_exists(role):
            return f"Error: Role '{role}' does not exist in the system. Try to choose existing role."
        
        # preparation of new records:
        userID = obj_database_keeper.generate_new_available_userID()
        new_record_for_users_table = [userID, username, role]

        new_hash = self.hash_password(password)
        new_record_for_secret_table = [userID, new_hash]
        
        # adding new records:
        obj_database_keeper.add_new_records_to_users_and_secret(new_record_for_users_table, new_record_for_secret_table)

        return f"A new user '{username}' created."

    def delete_user(self, username):
        obj_database_keeper = database_keeper.Database_Keeper()

        if not obj_database_keeper.is_this_user_exists_already(username):
            return f"User {username} seems not exist."

        userID = obj_database_keeper.get_userID_with_username(username)
        obj_database_keeper.delete_user_with_userID(userID)
        return f"User '{username}' deleted successfully."


