import socket
import time
from datetime import datetime
import json
import files_keeper #Do wywalenia? Nie używane i nie powinno tu być. Usunąć i sprawdzić czy nic się po tym nie zepsuje.
import server_operations
import user_manager
import message_box

START_TIME = time.time()
SERVER_VERSION = "1.0.0"
HOST = "127.0.0.1"
PORT = 65432

class Server_Main:
    def server(self):
        repeat = True
        session_user_ID = None
        session_username = None
        unique_session_number = None
        while repeat:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                print("Binduję połączenie.")
                s.bind((HOST, PORT))
                print("Nasłuchuję...")
                s.listen()
                print("Próbuję zaakceptować połączenie...")
                conn, addr = s.accept()
                print(f"Connected by {addr}")
                with conn:
                    while True:
                        data = conn.recv(1024)
                        if not data or data.decode('utf-8') == "":
                            break
                        command = data.decode('utf-8')
                        print(f"Received command: {command}")
                                                
                        if command == "stop":
                            obj_user_manager = user_manager.User_Manager()
                            if obj_user_manager.is_access_to_function_allowed(session_user_ID, "stop"):
                                self.func_stop(conn)
                                return None
                            else:
                                self.send_answer_back(conn, "The operation cannot be performed. Insufficient permissions.")
                                
                        
                        elif command == "uptime":
                            obj_user_manager = user_manager.User_Manager()
                            if obj_user_manager.is_access_to_function_allowed(session_user_ID, "uptime"):
                                obj_server_operations = server_operations.Server_operations()
                                answer = obj_server_operations.func_uptime(START_TIME)
                                self.send_answer_back(conn, answer)
                            else:
                                self.send_answer_back(conn, "The operation cannot be performed. Insufficient permissions.")
 
                        elif command == "info":
                            obj_user_manager = user_manager.User_Manager()
                            if obj_user_manager.is_access_to_function_allowed(session_user_ID, "info"):
                                obj_server_operations = server_operations.Server_operations()
                                answer = obj_server_operations.func_info(START_TIME, SERVER_VERSION)
                                self.send_answer_back(conn, answer)
                            else:
                                self.send_answer_back(conn, "The operation cannot be performed. Insufficient permissions.")
                            
                        elif command == "help":
                            obj_server_operations = server_operations.Server_operations()
                            answer = obj_server_operations.func_help()
                            self.send_answer_back(conn, answer)

                        elif command.startswith("show_user"):
                            row_username = command[len("show_user"):]
                            username = row_username.strip()
                            obj_user_manager = user_manager.User_Manager()
                            if obj_user_manager.is_access_to_function_allowed(session_user_ID, "show_user"):
                                self.send_answer_back(conn, obj_user_manager.show_user(username))
                            else:
                                self.send_answer_back(conn, "The operation cannot be performed. Insufficient permissions.")

                        elif command.startswith("create_user"):
                            words = command[len("create_user"):]
                            words = words.split()
                            obj_user_manager = user_manager.User_Manager()
                            if obj_user_manager.is_access_to_function_allowed(session_user_ID, "create_user"):
                                self.send_answer_back(conn, obj_user_manager.create_user(words[0], words[1], words[2]))
                            else:
                                self.send_answer_back(conn, "The operation cannot be performed. Insufficient permissions.")
                                
                        elif command.startswith("delete_user"):
                            words = command[len("delete_user"):]
                            words = words.split()
                            obj_user_manager = user_manager.User_Manager()
                            if obj_user_manager.is_access_to_function_allowed(session_user_ID, "delete_user"):
                                self.send_answer_back(conn, obj_user_manager.delete_user(words[0]))
                            else:
                                self.send_answer_back(conn, "The operation cannot be performed. Insufficient permissions.")

                        elif command.startswith("login"):
                            words = command[len("login"):]
                            words = words.split()
                            
                            obj_user_manager = user_manager.User_Manager()
                            result = obj_user_manager.log_in_user(words[0], words[1])
                            
                            back_message = result[0]
                            session_user_ID = result[1]
                            unique_session_number = result[2]
                            session_username = result[3]
                            
                            print(f"back_message: {back_message}")
                            print(f"session_user_ID: {session_user_ID}")
                            self.send_answer_back(conn, back_message)
                            print("User logged in successfully.")

                        elif command == "read_messages":
                            obj_user_manager = user_manager.User_Manager()
                            if obj_user_manager.is_access_to_function_allowed(session_user_ID, "read_messages"):
                                obj_message_box = message_box.Message_Box()
                                self.send_answer_back(conn, obj_message_box.read_messages(session_username))
                            else:
                                self.send_answer_back(conn, "The operation cannot be performed. Insufficient permissions.")

                        elif command == "delete_messages":
                            obj_user_manager = user_manager.User_Manager()
                            if obj_user_manager.is_access_to_function_allowed(session_user_ID, "delete_messages"):
                                obj_message_box = message_box.Message_Box()
                                self.send_answer_back(conn, obj_message_box.delete_messages(session_username))
                            else:
                                self.send_answer_back(conn, "The operation cannot be performed. Insufficient permissions.")

                        elif command.startswith("send_message"):
                            obj_user_manager = user_manager.User_Manager()
                            if obj_user_manager.is_access_to_function_allowed(session_user_ID, "send_message"):
                                words = command[len("send_message"):]
                                words = words.strip()
                                words = words.split(" ", 1)
                                receiver_name = words[0]
                                print(f"receiver_name = {receiver_name}")
                                text_message = words[1]
                                print(f"text_message = {text_message}")
                                obj_message_box = message_box.Message_Box()
                                self.send_answer_back(conn, obj_message_box.send_message(session_username, receiver_name, text_message))
                            else:
                                self.send_answer_back(conn, "The operation cannot be performed. Insufficient permissions.")
                        else:
                            conn.sendall(b"Unable to recognise the command.")
        print("Good bye!")
        
    def send_answer_back(self, conn, message):
        json_answer = json.dumps(message, indent=4)
        conn.sendall(bytes(json_answer, 'utf-8'))

    def func_stop(self, conn):
        self.send_answer_back(conn, "Stopping the server...")
    
obj_server = Server_Main()

obj_server.server() 
