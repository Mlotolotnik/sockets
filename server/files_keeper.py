import pathlib
import shutil
import json
import hashlib

class Files_Keeper:
    
    ### functions for handling the "users" file ###
    def check_whether_file_users_exists(self):
        return pathlib.Path("users").is_file()

    def get_userID_with_username_from_file_users(self, username):
        with open('users', 'r') as file:
            data = json.load(file)
            for line in data:
                if line[1] == username:
                    return line[0]
            return None
                
    def get_username_with_userID_from_file_users(self, userID):
        with open('users', 'r') as file:
            data = json.load(file)
            for line in data:
                if line[0] == userID:
                    return line[1]
            return None

    def get_username_role_homefolder_with_username_from_file_users(self, username):
        with open('users', 'r') as file:
            data = json.load(file)
            for line in data:
                if line[1] == username:
                    return line
            return None

    def get_role_with_userID_from_file_users(self, userID):
        with open('users', 'r') as file:
            data = json.load(file)
            for line in data:
                if line[0] == userID:
                    return line[2]
            return None
        
    def create_file_users_new_with_a_new_record(self, new_record):
        with open('users', 'r') as file:
            data = json.load(file)
            data.append(new_record)
            self.delete_file_users_new_if_exists()
            with open('users_new','w') as file_new:
                json.dump(data, file_new)
            
    def is_a_new_record_for_file_users_valid(self, new_record):
        ## Validation of the record:
        
        ## Record should be a list
        if type(new_record) is not list:
            print(f"Error: A new record should be a list but it is {type(new_record)}.")
            return False
      
        ## Record should have exactly 4 elements
        elif len(new_record) != 4:
            print(f"Error: A new record have {len(new_record)} items but it should have 4.")
            return False
        
        ## The first element (userID) should be type of 'int'
        elif type(new_record[0]) is not int:
            print(f"Error: A userID in a new record should be an 'int' but it is {type(new_record[0])}.")
            return False
        
        ## The first element (userID) should be different from existing ones
        elif self.is_this_userID_exists_already(new_record[0]):
            print(f"Error: Following userID: '{new_record[0]}' from a new record already exists in the system. Try to choose another userID.")
            return False
        
        ## The second element should be type of 'str'
        elif type(new_record[1]) is not str:
            print(f"Error: A type of 'username' should be str but it is {type(new_record[1])}")
            return False
        
        ## The third element should be type of 'str'
        elif type(new_record[2]) is not str:
            print(f"Error: A type of 'role' should be str but it is {type(new_record[2])}")
            return False
        
        ## The third element (role) should be a role existing in file 'roles' already
        elif not self.is_this_role_exists(new_record[2]):
            print(f"Error: There no such role in the system: {new_record[2]}")
            return False
        
        ## The fourth element (path) should be type of 'str'
        elif type(new_record[3]) is not str:
            print(f"Error: A type of 'path' should be str but it is {type(new_record[3])}")
            return False
        
        ## The fourth element (path) should be a path in correct convention: ./home/<username>
        elif new_record[3] != f"./home/{new_record[1]}":
            print(f"Error: The 'path' in a new record is invalid: '{new_record[3]}'")
            return False
        
        else:
            return True

    def is_this_userID_exists_already(self, userID):
        with open('users','r') as file_users:
            data_users = json.load(file_users)
            for line in data_users:
                if line[0] == userID:
                    return True
        with open('secret','r') as file_secret:
            data_secret = json.load(file_secret)
            for line in data_secret:
                if line[0] == userID:
                    return True
        return False

    def is_this_user_exists_already(self, username):
        userID = None
        with open('users','r') as file_users:
            data_users = json.load(file_users)
            for line in data_users:
                if line[1] == username:
                    userID = line[0]
                    
        with open('users','r') as file_users:
            data_users = json.load(file_users)
            for line in data_users:
                if line[0] == userID:
                    return True
        with open('secret','r') as file_secret:
            data_secret = json.load(file_secret)
            for line in data_secret:
                if line[0] == userID:
                    return True
        return False

    def generate_new_available_userID(self):
        new_userID_candidate = 1001
        while self.is_this_userID_exists_already(new_userID_candidate):
            new_userID_candidate = new_userID_candidate + 1
        return new_userID_candidate
        
    def get_record_from_users_with_userID(self, userID):
        with open("users","r") as file_users:
            data_users = json.load(file_users)
            for line in data_users:
                if userID == line[0]:
                    return line
        return None

    def delete_record_from_users_and_save_result_to_users_new(self, record):
        with open("users","r") as file_users:
            data_users = json.load(file_users)
            data_users.remove(record)
            with open("users_new","w") as file_users_new:
                json.dump(data_users, file_users_new)
        
    def rename_file_users_to_users_old(self):
        pathlib.Path("users").rename("users_old")
    
    def rename_file_users_new_to_users(self):
        pathlib.Path("users_new").rename("users")
        
    def delete_file_users_new_if_exists(self):
        if pathlib.Path("users_new").exists() and pathlib.Path("users_new").is_file():
            pathlib.Path("users_new").unlink()
        
    def delete_file_users_old_if_exists(self):
        if pathlib.Path("users_old").exists() and pathlib.Path("users_old").is_file():
            pathlib.Path("users_old").unlink()
        
    
    #functions for handling the "secret" file

##    def hash_password(self, password):
##        password_bytes = password.encode('utf=8')
##        hash_object = hashlib.sha256(password_bytes)
##        return hash_object.hexdigest()
    
    def check_whether_file_secret_exists(self):
        return pathlib.Path("secret").is_file()
    
    def get_hash_with_userID_from_file_secret(self, userID):
        with open('secret','r') as file_secret:
            data_secret = json.load(file_secret)
            for line in data_secret:
                if line[0] == userID:
                    return line[1]
        return None

    def create_file_secret_new_with_a_new_record(self, new_record):
        with open('secret','r') as file_secret:
            data_secret = json.load(file_secret)
            data_secret.append(new_record)
            self.delete_file_secret_new_if_exists()
            with open('secret_new','w') as file_new:
                json.dump(data_secret, file_new)



    def is_a_new_record_for_file_secret_valid(self, new_record):
        ## Validation of the record:

        ## Have to be a list
        if type(new_record) is not list:
            print(f"A new record should be a list but it is {type(new_record)}")
            return False
        
        ## Have to have exactly 2 elements
        if len(new_record) != 2:
            print(f"Error: The new record should have '2' elements but it have: '{len(new_record)}' elements.")
            return False
        
        ## The first element (userID) should be type of int
        elif type(new_record[0]) is not int:
            print(f"Error: 'userID' should be type 'int' but it is {type(new_record[0])}")
            return False
        
        ## The first element (userID) should be different from existing ones
        elif self.is_this_userID_exists_already(new_record[0]):
            print(f"Error: This 'userID' is in use already: {new_record[0]}. Try to choose another new 'userID'.")
            return False
        
        ## Second element should be type of 'str'
        elif type(new_record[1]) is not str:
            print(f"Error: Hash should be type 'str' but it is {type(new_record[1])}.")
            return False
        
        else:
            return True

    def get_record_from_secret_with_userID(self, userID):
        with open("secret","r") as file_secret:
            data_secret = json.load(file_secret)
            for line in data_secret:
                if line[0] == userID:
                    return line
        return None

    def delete_record_from_secret_and_save_result_to_secret_new(self, record):
        with open("secret","r") as file_secret:
            data_secret = json.load(file_secret)
            data_secret.remove(record)
            with open("secret_new","w") as file_secret_new:
                json.dump(data_secret, file_secret_new)
    
    def rename_file_secret_to_sercret_old(self):
        if pathlib.Path("secret").exists and pathlib.Path("secret").is_file():
            pathlib.Path("secret").rename("secret_old")
        
    def rename_file_secret_new_to_secret(self):
        if pathlib.Path("secret_new").exists and pathlib.Path("secret_new").is_file():
            pathlib.Path("secret_new").rename("secret")
        
    def delete_file_secret_new_if_exists(self):
        if pathlib.Path("secret_new").exists and pathlib.Path("secret_new").is_file():
            pathlib.Path("secret_new").unlink()

    def delete_file_secret_old_if_exists(self):
        if pathlib.Path("secret_old").exists and pathlib.Path("secret_old").is_file():
            pathlib.Path("secret_old").unlink()
    
    ### functions for handling the "roles" file ###
    def get_list_of_allowed_functions_with_a_rolename(self, role_name):
        with open('roles','r') as file_roles:
            data_roles = json.load(file_roles)
            for line in data_roles:
                if line[0] == role_name:
                    return line[1]
        print(f"No such role found: '{role_name}'.")
        return None

    def is_this_role_exists(self, role):
        with open('roles', 'r') as file:
            data = json.load(file)
            for line in data:
                if line[0] == role:
                    return True
        return False

##    def is_access_granted(self, userID, function_name): ### need test ###### need test ###### need test ###### need test ###### need test ###### need test ###
##  
##        with open('users', 'r') as file_users:
##            data_users = json.load(file_users)
##            for line in data_users:
##                if line[0] == userID:
##                    role_of_the_user = line[2]
##                    with open('roles', 'r') as file_roles:
##                        data_roles = json.load(file_roles)
##                        for line in data_roles:
##                            if line[0] == role_of_the_user:
##                                for line_role in line[1]:
##                                    if line_role == function_name:
##                                        return True
##        return False
    
    ### functions for handling the "./home/<username>/messages" file ###
    def is_users_home_directory_exists(self, username):
        return pathlib.Path(f"./home/{username}").is_dir()
        
    def is_users_messages_file_exists(self, username):
        return pathlib.Path(f"./home/{username}/messages").is_file()
          
    def remove_all_messages_from_messages_file(self, username):
        with open(f"./home/{username}/messages","w") as file_messages:
            data_messages = []
            json.dump(data_messages, file_messages)

    def is_message_box_format_is_valid(self, username):
        with open(f"./home/{username}/messages","r") as file_messages:
            try:
                data_messages = json.load(file_messages)
            except:
                return False
        return True

    def add_a_message_to_the_messages_file(self, from_who, to_who, message):
        data_messages = []
        with open(f"./home/{to_who}/messages","r") as file_messages:
            try:
                data_messages = json.load(file_messages)
            except:
                pass
            finally:
                message = f"Message from '{from_who}': " + message
                data_messages.append(message)
        with open(f"./home/{to_who}/messages","w") as file_messages:
            json.dump(data_messages, file_messages)

    def is_a_message_valid(self, message):
        ## Message validation:
        ## The message heve to be type of 'str':
        if type(message) is not str:
            print(f"The message should be type of 'str' but it is {type(message)}.")            
            return False
        ## The message length have to have 255 chars length or less:
        elif len(message) > 255:
            print(f"The length of the message is {len(message)} but it should be max 255 chars.")
            return False
        else:
            return True

    def get_all_messages_from_messages_file(self, username):
        with open(f"./home/{username}/messages", "r") as file_messages:
            data_messages = json.load(file_messages)
            return data_messages
        
    def is_messages_file_full(self, username, max_num_of_messages):
        with open(f"./home/{username}/messages", "r") as file_messages:
            data_messages = json.load(file_messages)
            if len(data_messages) >= max_num_of_messages:
                return True
            else:
                return False

    #################### Functions for handling './<username>/home' folder ####################

    def delete_homefolder_with_username(self, username):
        shutil.rmtree(f"./home/{username}")

    def is_homefolder_of_a_user_exists(self, username):
        home_path = f"./home/{username}"
        if pathlib.Path(home_path).exists() and pathlib.Path(home_path).is_dir():
            return True
        else:
            return False

    def create_homefolder(self, username):
        pathlib.Path(f"./home/{username}").mkdir(parents=True)



