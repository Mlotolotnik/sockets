import socket
import json

HOST = "127.0.0.1"
PORT = 65432  

class Client:
                
    def connection(self):
        client_obj_comm_with_user = Client_communication_with_user()
        client_obj_comm_with_server = Client_communication_with_server()
        while True:
            #communication with user:
            command = client_obj_comm_with_user.pick_a_command_from_user()
            if not client_obj_comm_with_user.validate_input_from_user(command):
                print("Invalid input.")
                continue
            #communication with server
            encoded_command = client_obj_comm_with_server.encode_command(command)
            received_raw_data = client_obj_comm_with_server.send_an_encoded_command_to_the_server_and_receive_an_answer(HOST, PORT, encoded_command)
            received_decoded_data = client_obj_comm_with_server.decode_an_answer_from_the_server(received_raw_data)
            if not client_obj_comm_with_server.validate_an_answer_from_the_server(received_decoded_data):
                print("Data received from the server, decoded and is invalid.")
                continue
            #communication with user
            client_obj_comm_with_user.print_decoded_data(received_decoded_data)
            if self.check_exit_condition_fulfilled(received_decoded_data):
                print("Good bye!")
                return None

    def check_exit_condition_fulfilled(self, answer_from_the_server):
        if answer_from_the_server == "\"Stopping the server...\"":
            return True
        else:
            return False


class Client_communication_with_user:
    
    def pick_a_command_from_user(self):
        while True:
            input_from_user = input("$ ")
            return input_from_user
        
    def validate_input_from_user(self, user_input):
        if type(user_input) is not str:
            return False
        else:
            return True

    def print_decoded_data(self, decoded_data):
        print(decoded_data)

    
class Client_communication_with_server:
    
    def encode_command(self, command):
        encoded_command = bytes(command, 'utf-8')
        return encoded_command
    
    def send_an_encoded_command_to_the_server_and_receive_an_answer(self, HOST, PORT, command):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                try:
                    s.connect((HOST, PORT))
                except:
                    print("Connection has been broken. Disconnected.")
                    
                s.sendall(command)
                raw_data = s.recv(1024)
                return raw_data

    def decode_an_answer_from_the_server(self, raw_data):
        decoded_data = raw_data.decode('utf-8')
        return decoded_data   

    def validate_an_answer_from_the_server(self, data):
        #validation process here, if data incorrect return False
        return True

client_obj = Client()

client_obj.connection()

