import unittest
import sys
sys.path.append("../server")
import user_manager

class Test_User_Manager(unittest.TestCase):

    def test_is_access_to_function_allowed(self):
        obj = user_manager.User_Manager()

        result = obj.is_access_to_function_allowed(1001, "create_user")
        self.assertTrue(result)

        result = obj.is_access_to_function_allowed(1001, "send_message")
        self.assertTrue(result)

        result = obj.is_access_to_function_allowed(1001, "create_blablabla")
        self.assertFalse(result)

        result = obj.is_access_to_function_allowed(1004, "create_user")
        self.assertFalse(result)
        
        result = obj.is_access_to_function_allowed(1004, "send_message")
        self.assertTrue(result)
        
        result = obj.is_access_to_function_allowed(1004, "create_blablabla")
        self.assertFalse(result)
                
    
    def test_log_in_user(self):
        obj = user_manager.User_Manager()

        result = obj.log_in_user("bobik", "bobik")
        expected1 = "User 'bobik' logged in successfully."
        expected2 = 1002
        self.assertEqual(result[0], expected1)
        self.assertEqual(result[1], expected2)

        result = obj.log_in_user("bobik", "blablalba")
        expected1 = "Error. Login Failed. Check your login and password and try again"
        expected2 = None
        self.assertEqual(result[0], expected1)
        self.assertEqual(result[1], expected2)

        result = obj.log_in_user("blablabla", "blablalba")
        expected1 = "Error. Login Failed. Check your login and password and try again"
        expected2 = None
        self.assertEqual(result[0], expected1)
        self.assertEqual(result[1], expected2)
        
    def test_create_show_delete_user(self):
        obj = user_manager.User_Manager()

        result = obj.create_user("shrek", "user", "shrek")
        expected = "A new user 'shrek' created."
        self.assertEqual(result, expected)

        result = obj.show_user("shrek")
        expected = "Username: 'shrek'. Role: 'user'."
        self.assertEqual(result, expected)

        result = obj.delete_user("shrek")
        expected = "User 'shrek' deleted successfully."
        self.assertEqual(result, expected)

        result = obj.show_user("shrek")
        expected = "Error: Unable to find such user: 'shrek'."
        self.assertEqual(result, expected)

        result = obj.create_user("bobik", "user", "blablabla")
        expected = "Error: User 'bobik' exists already. Try choose another username."
        self.assertEqual(result, expected)

        result = obj.create_user("barbie", "super_doll", "blablabla")
        expected = "Error: Role 'super_doll' does not exist in the system. Try to choose existing role."
        self.assertEqual(result, expected)

if __name__ == '__main__':
    unittest.main()
