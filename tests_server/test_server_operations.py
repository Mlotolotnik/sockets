import unittest
import sys
sys.path.append("../server")
import server_operations
import time
from datetime import datetime

class Test_Server_Operations(unittest.TestCase):

    def test_func_uptime(self):
        obj = server_operations.Server_operations()
        START_TIME = time.time()
        
        result = obj.func_uptime(START_TIME)
        expected = {'days': 0, 'hours': 0, 'minutes': 0, 'seconds': 0}
        self.assertEqual(result, expected) 

    def test_func_info(self):
        obj = server_operations.Server_operations()
        START_TIME = 1701188688.740029
        SERVER_VERSION = "1.0.0"

        result = obj.func_info(START_TIME, SERVER_VERSION)
        expected = {'date': {'year': 2023, 'month': 11, 'day': 28, 'hour': 17, 'minute': 24, 'second': 48}, 'server_version': '1.0.0'}
        self.assertEqual(result, expected)


    def test_func_help(self):
        obj = server_operations.Server_operations()
        
        result = obj.func_help()
        expected = {
            "help":"prints a list of commands",
            "login <username> <password>":"Try to login with username and password.",
            "info": "prints a version of the server and the time when it was started",
            "uptime": "prints how long the server is up",
            "stop": "stops the server",
            "show_user <username>": "shows info about user",
            "create_user <username> <role> <password>": "Creating a new user.",
            "delete_user <username>": "deleting a user",
            "read_messages":"Read messages from your message box.",
            "send_message <receiver_name> <message_to_sent>":"Sending a message to user's message box. Message cannot be longer than 255 characters.",
            "delete_messages":"Delete all messages from your message box."
            }
        self.assertEqual(result, expected)
        
if __name__ == '__main__':
    unittest.main()


