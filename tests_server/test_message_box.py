import unittest
import sys
sys.path.append("../server")
import message_box
import json

class Test_Message_Box(unittest.TestCase):

    def test_send_read_delete_read_message(self):
        ## Table 'messages' in database should empty when test starts
        ## preparation:
        obj = message_box.Message_Box()
        
        ## test 1:
        ## send one message to radek
        ## read messages from radek's message box
        ## check whether message correct
        obj.send_message("tomek", "radek", "message no 1.")
        result = obj.read_messages("radek")
        expected = "Message from 'tomek':\nmessage no 1.\nDate:"
        self.assertTrue(result.startswith(expected))
        
        ## test 2:
        ## send 4 more messages to radek
        ## send 1 more message to radek - should be error
        ## check whether there is error - message box is full
        obj.send_message("tomek", "radek", "message no 2.")
        obj.send_message("tomek", "radek", "message no 3.")
        obj.send_message("tomek", "radek", "message no 4.")
        obj.send_message("tomek", "radek", "message no 5.")
        result = obj.send_message("tomek", "radek", "message no 6.")
        expected = "Error: Unable to send the message. Messages box of a user 'radek' is full."
        self.assertEqual(result, expected)
        
        ## test 3:
        ## delete all messages belong to radek
        ## read messages from radek's message box
        ## check whether there is correct answer: "Your message box is empty."
        obj.delete_messages("radek")
        result = obj.read_messages("radek")
        expected = "Your message box is empty."
        self.assertEqual(result, expected)
        
        
if __name__ == '__main__':
    unittest.main()
